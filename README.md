# Web App From Scratch @cmda-minor-web 18-19

## Summary

![]()

## Table of contents
1. [Live demo](#Live-demo)
2. [Install](#Install)
3. [Features](#Features)
3. [Performance](#Performance)
8. [To-do](#To-do)
9. [Resources](#Resources)

## Live demo
[Click here](https://maybuzz.github.io/...) to see my live demo.

To install this project you'll have to fork this repository and open your terminal
```bash
  # insert your username to this link
  # put this in your terminal to clone the repo
  git clone https://github.com/your-user-name/.../
```

## Features

## Performance

## Enhancements

## To-do
- [x] `Express` `nodejs` server   
- [x] `.ejs` templating   
- [x] Serve data with json file   
- [ ] `Client side` vs `Server side` performance check   
-

## Resources
- [JSON with nodejs](https://stackoverflow.com/questions/12703098/how-to-get-a-json-file-in-express-js-and-display-in-view)   
- [Expressjs routing](http://expressjs.com/en/api.html#req.params)

## License
[MIT](LICENSE) © [Luna May Johansson](https://github.com/maybuzz)
